﻿using System.Collections;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Assets.Scripts;

//The posible positions of the player as an enum
enum Positions
{
    Left,
    Mid,
    Right
}

public class PlayerMovement : MonoBehaviour
{
    //[SerializeField] private float mainTimer;

    private float timer;
    private bool canCount = true;
    private bool doOnce = false;

    UnityEvent mEvent;
    UnityEvent mCornCollisionEvent;

    GameObject shield_obj;

    public GameObject particleEmiter;

    public GameObject Doggos;

    bool moving = false;

    bool doggosOn = false;

    // Active upgrades
    bool ShieldUnlocked = false;        //rabbit
    bool DoubleJumpUnlocked = false;    //sheep
    bool SpeedBoostUnlock = false;      //horse
    bool EggThrowUnlocked = false;      //chicken
    bool ShootingMilkUnlock = false;    //cow

    // Scene change
    public string changeToScene;
    public string changeToSceneLose;
    public Image image;
    public float fadeOutTime;
    public float messageFadeInTime;
    public Text deathMessage;
    public Text winMessage;

    // Jumping
    bool onGround = true;
    bool doubleJumpReady = false;
    public float CenterToGround;
    public float JumpForce = 100f;
    private bool particle = false;
    // Shield
    bool shieldOn = false;
    float ShieldRechargeTimeEllapsed = 0.0f;
    public float ShieldRechargeTime;
    public Text cooldownDisplay;

    // Shooting eggs
    bool eggOn = false;
    float EggRechargeTimeEllapsed = 0.0f;
    public float EggRechargeTime;
    public Text EggCooldownDisplay;
    public GameObject egg;

    // Getting milk
    bool milkOn = false;
    float MilkRechargeTimeEllapsed = 0.0f;
    public float MilkRechargeTime;
    float RealMilkRechargeTime = 0.0f;

    public float MilkDistance;
    public float MilkCurrTime;
    public float MilkMaxTime;
    public float MilkTimeFlow;
    public Image ui_back2;

    // Sideway movement
    float t = 0f;
    float maxT = 1f;
    public float speed = 2f;
    public float boostSpeed = 10f;
    public float hittingDistance = 0f;

    Positions position = Positions.Mid;     //Starts in the middle
    Rigidbody mRigidbody;

    Vector3 GoTo = new Vector3(0f, 0.7f, -7f);
    Vector3 prevPos = new Vector3(0f, 0f, 0f);

    public Quaternion camRot;

    // Start is called before the first frame update
    void Start()
    {
        // Get the rigid body
        mRigidbody = GetComponent<Rigidbody>();

        // Find the Shield object and initialize the death message 
        shield_obj = transform.Find("Shield").gameObject;
        shield_obj.SetActive(ShieldUnlocked);
        deathMessage.gameObject.SetActive(false);

        // Create and set the callback funcitons for the events (launched internaly)
        // Mostly it is not necessay to do the logic this way, but it allow for other
        // objects to interact with the Player throught the same callback functions
        // if needed later for their own events.
        if (mEvent == null)
            mEvent = new UnityEvent();
        if (mCornCollisionEvent == null)
            mCornCollisionEvent = new UnityEvent();

        camRot = Camera.main.gameObject.transform.rotation;

        //mEvent.AddListener(CollisionFn);
        mCornCollisionEvent.AddListener(OnCornCollision);
        mEvent.AddListener(PlayCollisionSound);
    }

    // Update is called once per frame. A lot of game logic goes here
    void Update()
    {
        //Timer for doggos
        if (doggosOn && timer <= 0.0f)
        {
            timer = 5.0f;
            canCount = true;
            doOnce = false;
        }

        if (timer >= 0.5f && canCount)
        {
            timer -= Time.deltaTime;
        }
        else if (timer <= 0.5f && !doOnce) {
            canCount = false;
            doOnce = true;
            timer = 0.0f;
            doggosOn = false;
            Vector3 newpos = new Vector3(Doggos.transform.position.x,
                                         Doggos.transform.position.y,
                                         -12.5f);
            Doggos.transform.position = newpos;
        }

        //moving "animation"
        gameObject.transform.rotation = new Quaternion(-0.01f * Mathf.Sin(Mathf.Deg2Rad * Time.frameCount* 5), gameObject.transform.rotation.y, gameObject.transform.rotation.z, gameObject.transform.rotation.w);
        Camera.main.gameObject.transform.rotation = camRot;

        

        if (particle)
        {
            particleEmiter.SetActive(true);
        }
        else
        {
            particleEmiter.SetActive(false);
        }

        // Check collisions in the front with raycast
        RaycastHit hit;
        if (Physics.Raycast(transform.position + new Vector3(0f, 0.5f, 0f), new Vector3(0f, 0f, 1f), out hit))
        {
            if (Mathf.Abs(hit.transform.position.z - transform.position.z) < hit.transform.localScale.z / 2 + transform.localScale.z / 2 + hittingDistance)
            {
                mEvent.Invoke();
                if (hit.transform.gameObject.name != "Model_Coin")
                {
                    mCornCollisionEvent.Invoke();
                    Destroy(hit.transform.gameObject);
                }
            }
        }

        if (Physics.Raycast(transform.position + new Vector3(0f, 0, 0f), new Vector3(0f, 1, 0f), out hit))
        {
            if (Mathf.Abs(hit.transform.position.z - transform.position.z) < Mathf.Epsilon)
            {
                if (hit.transform.gameObject.tag == "Death")
                {
                    mCornCollisionEvent.Invoke();
                    Destroy(hit.transform.gameObject);
                }
            }
        }

        // Get input for sideway moving
        if (Input.GetKeyDown(KeyCode.A) && position != Positions.Left && !moving)
        {
            prevPos = transform.position;
            GoTo    = new Vector3(-GlobalGameProperties.LaneDistance, 0f, 0f);

            position -= 1;      // Go Left / Reduces the position

            moving = true;
        }
        else if (Input.GetKeyDown(KeyCode.D) && position != Positions.Right && !moving)
        {
            prevPos = transform.position;
            GoTo    = new Vector3(GlobalGameProperties.LaneDistance, 0f, 0f);

            position += 1;      // Go Right / Increase the position

            moving = true;
        }

        //Movement of the player (Assuming it only moves on the x !!!)
        if (moving)
        {
            if (SpeedBoostUnlock)
                speed = boostSpeed;

            // Move just in the axis given by GoTo (should be the X axis only, 
            // therefore jumping is completely independent)
            transform.position += GoTo * Time.deltaTime * speed;

            t += Time.deltaTime * speed;        // Keep track of the distance traveled (time lapsed)
            if (t > maxT)                       
            {
                // Make sure the target position has been reached completely, 
                // otherwise it is lag dependent (or even random)
                transform.position.Set(prevPos.x + GoTo.x, transform.position.y, transform.position.z);   

                t = 0f;                         // Reset the timer and stop moving
                moving = false;
            }
        }

        if(!moving)
        {
            switch(position)
            {
                case Positions.Left:
                    transform.position = new Vector3(-GlobalGameProperties.LaneDistance, transform.position.y, transform.position.z);
                    break;

                case Positions.Mid:
                    transform.position = new Vector3(0f, transform.position.y, transform.position.z);
                    break;

                case Positions.Right:
                    transform.position = new Vector3(GlobalGameProperties.LaneDistance, transform.position.y, transform.position.z);
                    break;
            }
        }

        // Jump
        onGround = IsTouching(-Vector3.up, CenterToGround, "Ground");           // Check if touching Ground (from above)

        if (onGround && !doubleJumpReady && DoubleJumpUnlocked)  doubleJumpReady = true;    // If on Ground, can reset double jump
        if (Input.GetKeyDown(KeyCode.Space) && (onGround || doubleJumpReady))               // If asked to jump, must be on the Ground or have double jump ready
        {
            if (!onGround) doubleJumpReady = false;                             // If not on ground, consume the double jump

            mRigidbody.velocity = new Vector3(mRigidbody.velocity.x, 0.0f, mRigidbody.velocity.z);      // Reset velocity on y, so that the double jump is always the same
            mRigidbody.AddForce(new Vector3(0, 1, 0) * JumpForce, ForceMode.Impulse);                   // Actual Jump
        }

        // Shield Recharge
        if (!shieldOn && ShieldUnlocked)
        {
            ShieldRechargeTimeEllapsed += Time.deltaTime;

            if (ShieldRechargeTimeEllapsed < ShieldRechargeTime)
                cooldownDisplay.text = "Shield Cooldown: " + (ShieldRechargeTime - ShieldRechargeTimeEllapsed).ToString() + "s";
            else
                cooldownDisplay.text = "Shield Cooldown: Ready! (Press W)";

            if (Input.GetKeyDown(KeyCode.W) && ShieldRechargeTimeEllapsed > ShieldRechargeTime)
            {
                cooldownDisplay.text = "";
                ShieldRechargeTimeEllapsed = 0.0f;
                shieldOn = true;
                shield_obj.SetActive(true);
            }
        }

        // Egg Throw
        if(!eggOn && EggThrowUnlocked && !ShootingMilkUnlock)
        {
            EggRechargeTimeEllapsed += Time.deltaTime;

            if (EggRechargeTimeEllapsed < EggRechargeTime)
                EggCooldownDisplay.text = "Egg Throw Cooldown: " + (EggRechargeTime - EggRechargeTimeEllapsed).ToString() + "s";
            else
                EggCooldownDisplay.text = "Egg Throw: Ready! (Press Q)";

            if (onGround && Input.GetKeyDown(KeyCode.Q) && EggRechargeTimeEllapsed > EggRechargeTime)
            {
                EggCooldownDisplay.text = "";
                EggRechargeTimeEllapsed = 0.0f;
                eggOn = false;

                // Actual logic of throwing a thing
                Instantiate(egg, gameObject.transform.position + gameObject.transform.forward * 1.5f + gameObject.transform.up * 0.75f, egg.transform.rotation);
            }
        }

        // Need this for throwing right Raycast towards the Fences in shooting milk
        Vector3 yes_pos = transform.position;
        yes_pos.y += transform.localScale.y / 2;

        Debug.DrawRay(yes_pos, new Vector3(0f, yes_pos.y, MilkDistance), Color.red);

        // If the key Q is up at any moment
        if (!Input.GetKey(KeyCode.Q) && MilkCurrTime > 0.0f)
            // Decrease the current distance of the hosed milk
            MilkCurrTime -= Time.deltaTime * MilkTimeFlow;

        if (MilkCurrTime < 0.0f)
            MilkCurrTime = 0.0f;

        // Milk Unlock
        if (!milkOn && ShootingMilkUnlock)
        {
            ui_back2.enabled = true;

            MilkRechargeTimeEllapsed += Time.deltaTime;

            if (MilkRechargeTimeEllapsed < RealMilkRechargeTime)
                EggCooldownDisplay.text = "Milk Throw Cooldown: " + (RealMilkRechargeTime - MilkRechargeTimeEllapsed).ToString() + "s";
            else
            {
                EggCooldownDisplay.text = "Milk Throw: Ready! (Press Q)";
                RealMilkRechargeTime = 0.0f;
            }

            if (Input.GetKey(KeyCode.Q) && MilkRechargeTimeEllapsed >= RealMilkRechargeTime)
            {
               // particleEmiter.gameObject.SetActive(true);
              //  particleEmiter.GetComponent<ParticleSystem>().Play();

                // If Q is pressed, the curr time increases
                MilkCurrTime += Time.deltaTime * MilkTimeFlow;
                particle = true;
                if (MilkCurrTime >= MilkMaxTime)
                {
                    MilkCurrTime = MilkMaxTime;
                    RealMilkRechargeTime = MilkRechargeTime;
                    MilkRechargeTimeEllapsed = 0.0f;
                    return;
                }

                EggCooldownDisplay.text = "";
                MilkRechargeTimeEllapsed = 0.0f;
                milkOn = false;


                // Need this for throwing right Raycast towards the Fences in shooting milk
                Vector3 new_pos = transform.position;
                new_pos.y += transform.localScale.y / 2;

                // Check collisions in the front with raycast
                RaycastHit hit_fence;
                if (Physics.Raycast(new_pos, new Vector3(0f, new_pos.y, 1000.0f), out hit_fence))
                {
                    if (Mathf.Abs(hit_fence.transform.position.z - transform.position.z) < MilkDistance)
                    {
                        if (hit_fence.collider.CompareTag("Fence"))
                        {
                            // Play the sound
                            if (!FindObjectOfType<AudioManager>().IsPlaying("Crash"))
                            {
                                FindObjectOfType<AudioManager>().Play("Crash");
                            }

                            //mCornCollisionEvent.Invoke();
                            Destroy(hit_fence.transform.gameObject);
                        }
                    }
                }
            }
            else
            {

                particle = false;
                /* particleEmiter.gameObject.SetActive(false);
                 particleEmiter.GetComponent<ParticleSystem>().Stop();*/
            }
        }
        else
            particle = false;
    }

    bool IsTouching(Vector3 direction, float _distance, string _needed_tag)
    {
        // Look for the first object in the direction of the ray
        if (Physics.Raycast(transform.position, direction, out RaycastHit hitInfo, _distance))
        {
           // If found, check if contains the requested tag
            return hitInfo.collider.CompareTag(_needed_tag);
        }

        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        mEvent.Invoke();
        if (other.gameObject.name == "Model_Coin")
            return;
        mCornCollisionEvent.Invoke();
    }

    /////////////////////////////////// Event Callback functions ///////////////////////////////////

    void PlayCollisionSound()
    {

        RaycastHit hit;
        if (!Physics.Raycast(transform.position + new Vector3(0f,0.5f,0f), new Vector3(0f, 0f, 1f), out hit))
            return;

        if(hit.transform.gameObject.name == "Model_Coin")
        {
            if (!FindObjectOfType<AudioManager>().IsPlaying("Coin"))
            {
                FindObjectOfType<AudioManager>().Play("Coin");
            }
            return;
        }

        if (!FindObjectOfType<AudioManager>().IsPlaying("Crash"))
        {
            FindObjectOfType<AudioManager>().Play("Crash");
        }
    }

    void OnCornCollision()
    {
        if (shieldOn)
        {
            shieldOn = false;
            shield_obj.SetActive(false);
        }
        else if (doggosOn == false)
        {
            doggosOn = true;
            Vector3 newpos = new Vector3(Doggos.transform.position.x, 
                                         Doggos.transform.position.y, 
                                         -9.5f);
            Doggos.transform.position = newpos;
        }
        else
        {
            deathMessage.gameObject.SetActive(true);
            StartCoroutine(FadeOutChangeScene());
        }
    }

    public void OnShieldUnlocked()
    {
        ShieldUnlocked = true;

        if (!FindObjectOfType<AudioManager>().IsPlaying("PowerUp"))
        {
            FindObjectOfType<AudioManager>().Play("PowerUp");
        }
    }
    public void OnDoubleJumpUnlocked()
    {
        DoubleJumpUnlocked = true;

        if (!FindObjectOfType<AudioManager>().IsPlaying("PowerUp"))
        {
            FindObjectOfType<AudioManager>().Play("PowerUp");
        }
    }
    public void OnSpeedBoostUnlock()
    {
        SpeedBoostUnlock = true;

        if (!FindObjectOfType<AudioManager>().IsPlaying("PowerUp"))
        {
            FindObjectOfType<AudioManager>().Play("PowerUp");
        }
    }
    public void OnShootingMilkUnlock()
    {
        ShootingMilkUnlock = true;
        
        if (!FindObjectOfType<AudioManager>().IsPlaying("PowerUp"))
        {
            FindObjectOfType<AudioManager>().Play("PowerUp");
        }
    }
    public void OnEggThrowUnlocked()
    {
        EggThrowUnlocked = true;

        if (!FindObjectOfType<AudioManager>().IsPlaying("PowerUp"))
        { 
            FindObjectOfType<AudioManager>().Play("PowerUp");
        }
    }
    public void OnWin()
    {
        StartCoroutine(FadeOutWin());
    }

    ///////////////////////////////////      Scene Changing      ///////////////////////////////////

    IEnumerator FadeOutChangeScene()
    {
        // Message ("You Lose" or whatever) fades in. Notice that if the time 
        // is >1s, opacity will be filling for the 1st second and then the object 
        // will remain with full opacity

        if (!FindObjectOfType<AudioManager>().IsPlaying("Lose"))
        {
            FindObjectOfType<AudioManager>().Play("Lose");
        }

        if (FindObjectOfType<AudioManager>().IsPlaying("MainTheme"))
        {
            FindObjectOfType<AudioManager>().Stop("MainTheme");
        }

        for (float i = 0; i < messageFadeInTime; i += Time.deltaTime)
        {
            deathMessage.color = new Color(1, 0, 0, i);
            yield return null;
        }

        // Same explanation as before but for the actual fader
        for (float i = 0; i < fadeOutTime; i += Time.deltaTime)
        {
            image.color = new Color(0, 0, 0, i);
            yield return null;
        }

        // Start loading the Scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(changeToSceneLose);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    IEnumerator FadeOutWin()
    {
        // Message ("You Win" or whatever) fades in. Notice that if the time 
        // is >1s, opacity will be filling for the 1st second and then the object 
        // will remain with full opacity

        if (!FindObjectOfType<AudioManager>().IsPlaying("Win"))
        {
            FindObjectOfType<AudioManager>().Play("Win");
        }

        if (FindObjectOfType<AudioManager>().IsPlaying("MainTheme"))
        {
            FindObjectOfType<AudioManager>().Stop("MainTheme");
        }

        for (float i = 0; i < messageFadeInTime; i += Time.deltaTime)
        {
            winMessage.color = new Color(0, 1, 0, i);
            yield return null;
        }

        // Same explanation as before but for the actual fader
        for (float i = 0; i < fadeOutTime; i += Time.deltaTime)
        {
            image.color = new Color(0, 0, 0, i);
            yield return null;
        }

        // Start loading the Scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(changeToScene);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

}
