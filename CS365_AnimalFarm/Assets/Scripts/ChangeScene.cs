﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ChangeScene : MonoBehaviour
{
    public Button button;
    public Button quitButton;
    public Button TutotialButton;
    public string changeToScene;
    public Image image;
    public float fadeOutTime;

    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(onClick);
        TutotialButton.onClick.AddListener(onClick);
        quitButton.onClick.AddListener(OnQuitClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void onClick()
    {
        StartCoroutine(FadeOutChangeScene());
    }

    void OnQuitClick()
    {
        Application.Quit();
    }

    public void OnTutorialClick() {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Tutorial");
    }

    IEnumerator FadeOutChangeScene()
    {
        for(float i = 0; i < fadeOutTime; i += Time.deltaTime)
        {
            image.color = new Color(0, 0, 0, i);
            yield return null;
        }

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(changeToScene);
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
