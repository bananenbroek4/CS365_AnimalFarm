﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// DSICLAIMER: It probably only supports one simultaneous notification ///

[RequireComponent(typeof(Text))]

public class Yell : MonoBehaviour
{
    float mFadeTime;
    Text mTextComp;

    private void Start()
    {
        mTextComp = GetComponent<Text>();
    }

    public void Notify(string _text, float _fade_in_out)
    {
        mTextComp.text = _text;
        mFadeTime = _fade_in_out;

        StartCoroutine(ActualYell());
    }

    public IEnumerator ActualYell()
    {
        for (float i = 0; i < mFadeTime; i += Time.deltaTime)
        {
            mTextComp.color = new Color(mTextComp.color.r, mTextComp.color.g, mTextComp.color.b, i / mFadeTime);
            yield return null;
        }
        for (float i = 0; i < mFadeTime; i += Time.deltaTime)
        {
            mTextComp.color = new Color(mTextComp.color.r, mTextComp.color.g, mTextComp.color.b, 1 - i / mFadeTime);
            yield return null;
        }
        mTextComp.text = "";
    }
}
