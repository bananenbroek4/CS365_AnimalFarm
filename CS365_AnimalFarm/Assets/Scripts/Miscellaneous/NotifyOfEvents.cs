﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NotifyOfEvents : MonoBehaviour
{
    Yell mYell;

    // Start is called before the first frame update
    void Start()
    {
        mYell = GetComponent<Yell>();
    }

    // Update is called once per frame
    public void OnShieldUnlocked()
    {
        mYell.Notify("Shield Unlocked", 1.0f);
    }
    public void OnDoubleJumpUnlocked()
    {
        mYell.Notify("Double Jump Unlocked", 1.0f);
    }

    public void OnSpeedBoostUnlock()
    {
        mYell.Notify("Speed Boost Unlocked", 1.0f);
    }
    public void OnShootingMilkUnlock()
    {
        mYell.Notify("Shooting Milk Unlocked", 1.0f);
    }
    
    public void OnEggThrowUnlocked()
    {
        mYell.Notify("Egg Throw Unlocked", 1.0f);
    }

    public void  OnFhase1()
    {
        mYell.Notify("Use A D to move to the sides", 1.0f);
    }

    public void OnJumpTutorial()
    {
        mYell.Notify("Use SPACE to jump", 1.0f);
    }

    public void OnFhase2()
    {
        mYell.Notify("Move to dodge and collect coins", 1.0f);
    }

    public void OnPowerUp()
    {
        mYell.Notify("Unlock powerUps by collecting coins", 1.0f);
    }
}
