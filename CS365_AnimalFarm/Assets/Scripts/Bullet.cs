﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody rb;
    public CapsuleCollider col;

    public float velocity;
    public float distance_to_fence;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = new Vector3(0, 0, velocity);
    }

    // Update is to just move in some place
    void Update()
    {
        // Collision with the Fence
        if(Physics.Raycast(transform.position, rb.velocity, out RaycastHit hitInfo, distance_to_fence))
        {
            if(hitInfo.collider.CompareTag("Fence"))
            {
                if (!FindObjectOfType<AudioManager>().IsPlaying("Crash"))
                {
                    FindObjectOfType<AudioManager>().Play("Crash");
                }

                //If the GameObject's name matches the one you suggest, output this message in the console
                Destroy(hitInfo.collider.gameObject);
                Destroy(gameObject);
            }
        }
    }
}