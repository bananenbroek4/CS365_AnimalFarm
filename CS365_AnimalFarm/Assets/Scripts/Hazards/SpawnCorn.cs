﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Assets.Scripts;

public class SpawnCorn : MonoBehaviour
{
    public float minTimeOfSpawn = 1.0F;
    public float maxTimeOfSpawn = 3.0F;
    public float height = 10.0F;
    public GameObject corn;
    public GameObject target;
    //public Vector3[] positions;
    private float timer;
    private float player_z;

    bool bTutorial = false;

    // Start is called before the first frame update
    void Start()
    {
        timer = Random.Range(minTimeOfSpawn, maxTimeOfSpawn);
        GameObject player = GameObject.Find("Player");
        player_z = player.transform.position.z;

        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;
        if (sceneName == "Tutorial")
        {
            bTutorial = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!bTutorial)
        {
            timer -= Time.deltaTime;
            if (timer <= 0.0F)
            {
                timer = Random.Range(minTimeOfSpawn, maxTimeOfSpawn);

                Vector3 lane_pos_translation = new Vector3(0.0f, height, player_z);
                int lane_offset_int = Random.Range(-1, 2);
                lane_pos_translation.x = lane_offset_int * GlobalGameProperties.LaneDistance;

                Instantiate(corn, lane_pos_translation, new Quaternion());

                //marking on the point it is going to fall at
                target.transform.position = new Vector3(lane_pos_translation.x, target.transform.position.y, lane_pos_translation.z);
                target.GetComponent<SpriteRenderer>().enabled = true;
            }
        }
    }

    void onMovmentTutorialEnd()
    {
        bTutorial = false;
    }
}
