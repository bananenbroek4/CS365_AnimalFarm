﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnSickle : MonoBehaviour
{
    
    public GameObject sickle;
    public GameObject player;
    public float randTimeMin;
    public float randTimeMax;

    public float MaxTimeThrowing;
    public int MinFrames;
    public float coolDownTime;
    bool spawn;
    bool coolDownDone;
    float checkTime;
    float counter;

    float[] ScyTime = new float[6];
    int n = 0;

    float time = 0;

    bool bTutorial = false;

    //public Raycast raycast;
    // Start is called before the first frame update
    void Start()
    {
        checkTime = Random.Range(randTimeMin, randTimeMax);
        spawn = false;
        coolDownDone = true;

        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;
        if (sceneName == "Tutorial")
        {
            bTutorial = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!bTutorial)
        {
            counter += Time.deltaTime;
            transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);

            if (counter >= coolDownTime && !coolDownDone && !spawn)
            {
                counter = 0.0f;
                coolDownDone = true;
            }
            if (counter >= checkTime && coolDownDone && !spawn)
            {
                counter = 0.0f;
                spawn = true;
                float minimu = 0;
                n = 0;

                //random time for each  between two margains(?)
                for (int i = 0; i < 6; i++)
                {
                    //Timer
                    ScyTime[i] = Random.Range(minimu, MaxTimeThrowing + MinFrames * n * Time.deltaTime);
                    minimu = ScyTime[i] + Time.deltaTime;
                }

            }

            if (Physics.Linecast(transform.position, player.transform.position) && spawn)
            {

                print("Have to spawn");

                if (time >= ScyTime[n])
                {
                    print("Spawning " + n);
                    Throw();
                    n++;
                }

                time += Time.deltaTime * MinFrames;
            }

            if (n >= 6)
            {
                checkTime = Random.Range(randTimeMin, randTimeMax);
                spawn = false;
                counter = 0.0f;
                n = 0;
                time = 0;
            }
        }
    }

    void Throw()
    {
        var syc = Instantiate(sickle, player.transform.position - new Vector3(0, -0.1f, 3.23f), transform.rotation);
        syc.GetComponent<Rigidbody>().AddForce(0, 400, 100);
        syc.GetComponent<Rigidbody>().AddTorque(50, 0, 0);
    }

    public void onMovmentTutorialEnd()
    {
        bTutorial = false;
    }
}
