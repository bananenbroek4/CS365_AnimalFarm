﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts;


public class Hazard : MovingObject
{
    public bool NO = false;
    public static List<GameObject> sAllHazards = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        int insertAtIndex = sAllHazards.Count;
        if (NO)
        {
            Hazard.sAllHazards.Insert(insertAtIndex, gameObject);
            return;
        }
        AddMovingObject();

        // Insert into list in sorted manner
        for(; insertAtIndex > 0; --insertAtIndex)
        {
            if (Hazard.sAllHazards[insertAtIndex - 1].transform.position.z < gameObject.transform.position.z)
                break;
        }
        Hazard.sAllHazards.Insert(insertAtIndex, gameObject);

        for(int i = 0; i < sAllHazards.Count; ++i)
        {
            sAllHazards[i].tag = "Fence";
        }
    }

    private void OnDestroy()
    {
          RemoveMovingObject();
        Hazard.sAllHazards.Remove(gameObject);
    }
}
