﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentLooper : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Environment>(out Environment hazard_comp))
        {
            hazard_comp.gameObject.transform.position += new Vector3(0, 0, 40);
            //Destroy(hazard_comp.gameObject);
        }
    }
}
