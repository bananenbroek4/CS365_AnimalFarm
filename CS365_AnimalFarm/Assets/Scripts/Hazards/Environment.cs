﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts;

public class Environment : MonoBehaviour
{
    Rigidbody mRigid;
    GameObject player;
    Vector3 initialPos;
    // Start is called before the first frame update
    void Start()
    {
        mRigid = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");

        /*For some reason Unity terrain's editor's Transform doesn't show the real value
          but a (I think) normalized one, so by trial and error I got that these three
          numbers are the correct ones */
        initialPos = new Vector3(-584.2f, -0.3f, -50.0f);
    }

    // Update is called once per frame
    void Update()
    {
        mRigid.velocity = new Vector3(0, 0, -GlobalGameProperties.HazardMoveSpeed);
        //gameObject.GetComponent<Transform>().Translate(0.0f, 0.0f, -GlobalGameProperties.HazardMoveSpeed * Time.deltaTime);

        if (gameObject.name == "Terrain")
        {
            if (gameObject.transform.position.z < -990.0f)
                gameObject.transform.position = initialPos;
        }
    }
}
