﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts;

public class MovingObject : MonoBehaviour
{

    static public List<GameObject> sMovingObjects = new List<GameObject>();

    // Start is called before the first frame update
    
    void Start()
    {
        AddMovingObject();
    }

    private void OnDestroy()
    {
        MovingObject.sMovingObjects.Remove(gameObject);
    }

    protected void AddMovingObject()
    {
        // Insert into list in sorted manner
        int insertAtIndex = sMovingObjects.Count;
        for (; insertAtIndex > 0; --insertAtIndex)
        {
            if (MovingObject.sMovingObjects[insertAtIndex - 1].transform.position.z < gameObject.transform.position.z)
                break;
        }
        MovingObject.sMovingObjects.Insert(insertAtIndex, gameObject);
    }
    protected void RemoveMovingObject()
    {
        MovingObject.sMovingObjects.Remove(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Transform transform = gameObject.GetComponent<Transform>();
        Vector3 new_pos = transform.position;
        new_pos += new Vector3(0.0f, 0.0f, -GlobalGameProperties.HazardMoveSpeed * Time.deltaTime);
        transform.position = new_pos;
        
    }
}
