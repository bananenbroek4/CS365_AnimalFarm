﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts;
using UnityEngine.Events;

public class CornBehaviour : MonoBehaviour
{
    public float hittingDistance;

    void Start()
    {

    }
    void Update()
    {
        //If game is stopped, corn destroys itself.
        if (GlobalGameProperties.HazardMoveSpeed == 0)
        {
            GameObject.Find("mark").GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject);
        }

        if (transform.position.y < -transform.localScale.y / 2)
        {
            GameObject.Find("mark").GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject);
        }

    }
}