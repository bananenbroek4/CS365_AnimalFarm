﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Hazards
{
    /* \class   Pattern
     * 
     * \brief   This script is attached to an object to which several hazards are attached to.
     *          When a pattern is "spawned in" by the hazard generator, the "pattern object"
     *          (i.e. the object that owns this script) immediately detaches all its children before
     *          destroying itself, so that all its children are attached to the world instead.
     * 
     *          A pattern will *always* be spawned in the central lane, so all children hazards must be
     *          defined around this central pattern object.
     */
    class Pattern : MonoBehaviour
    {
        // Allow hazard generator to flip objects along axis perpendicular to lanes.
        //[SerializeField]
        //public bool AllowPerpendicularMirror = false;

        // Allow hazard generator to flip objects along axis parallel to lanes.
        [SerializeField]
        public bool AllowParallelMirror = false;

        // Enabling this will only allow the pattern to spawn in the middle lane.
        // Useful for spawning hazards relative to a center hazard pattern object.
        [SerializeField]
        public bool MiddleLaneOnly = true;

        // Determines how often this pattern will be selected for spawning.
        [SerializeField]
        public uint Weight = 5;

        void Start()
        {
            // Detach all children (i.e. attach to world transform)
            for(int i = 0; i < gameObject.transform.childCount; ++i)
            {
                Transform child_transform = gameObject.transform.GetChild(i);
                Vector3 new_pos = gameObject.transform.position;
                child_transform.parent = null;

                if (new_pos.x >= 1.0f)
                    new_pos.x = GlobalGameProperties.LaneDistance;
                else if (new_pos.x <= -1.0f)
                    new_pos.x = -GlobalGameProperties.LaneDistance;
                else
                    new_pos.x = 0.0f;

                if (AllowParallelMirror)
                    new_pos.x *= -1.0f;

                child_transform.position = new_pos;
            }

            // Destroy self immediately.
            Destroy(gameObject);
        }
    }
}
