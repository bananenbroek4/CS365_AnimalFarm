﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SickleBehavior : MonoBehaviour
{
    public float torque;
    public Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddTorque(transform.up * torque);
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
