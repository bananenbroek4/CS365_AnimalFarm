﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Assets.Scripts;
using Assets;

/*  \class      HazardGenerator
 *  \brief      This system is in charge of using "pattern" prefabs
 *              utilizing the Pattern script in order procedurally
 *              generate hazards.
 *              Patterns are defined by the user.
 */
public class PatternGenerator : MonoBehaviour
{
    public GameObject[] SpawnableHazardPatterns;
    float sTotalWeight = 0.0f;

    bool bTutorial = false;
    bool bTutorialJump = false;
    bool bCoinCollectionTutorial = false;
    int fenceCount = 0;
    int[] Phase1 = {1, 0, -1, 0, 1, 0, -1, 1, 1, 0};
    int JumpCount = 0;

    public UnityEvent onMovementTutorialFinish;
    public UnityEvent onJumpTutorial;

    // Start is called before the first frame update
    void Start()
    {
        for(uint i = 0; i < SpawnableHazardPatterns.Length; ++i)
            sTotalWeight += SpawnableHazardPatterns[i].GetComponent<Assets.Scripts.Hazards.Pattern>().Weight;
    }

    // Update is called once per frame
    void Update()
    {
        if (SpawnableHazardPatterns.Length == 0) return;
        if (MovingObject.sMovingObjects.Count < GlobalGameProperties.SpawnPatternBelowObjectTreshhold)
            if (bTutorial)
                TutorialPattern();
            else
                SpawnPattern();

        if (bCoinCollectionTutorial)
        {
            SpawnCoins();
        }
    }

    private void TutorialPattern()
    {
        if (fenceCount < 10)
        {
            Vector3 new_hazard_pos;
            if (MovingObject.sMovingObjects.Count != 0)
                new_hazard_pos.z = MovingObject.sMovingObjects[MovingObject.sMovingObjects.Count - 1].transform.position.z;
            else
                new_hazard_pos.z = gameObject.transform.position.z;
            new_hazard_pos.z += GlobalGameProperties.MinimumPatternIntervalDistance;

            new_hazard_pos.x = 0.0f;
            new_hazard_pos.y = 0.0f;

            uint patterns = PickPatternIndex();
            if (patterns == 1) patterns = 0;
            GameObject clone_hazard_pattern_prefab = SpawnableHazardPatterns[patterns];

            if (clone_hazard_pattern_prefab)
            {
                Assets.Scripts.Hazards.Pattern pattern_script = null;
                bool result = clone_hazard_pattern_prefab.TryGetComponent<Assets.Scripts.Hazards.Pattern>(out pattern_script);
                if (result)
                {
                    int lane_offset_int = Phase1[fenceCount];
                    new_hazard_pos.x = lane_offset_int * GlobalGameProperties.LaneDistance;
                }

                Instantiate(
                    clone_hazard_pattern_prefab,
                    new_hazard_pos,
                    Quaternion.identity
                );
            }

            fenceCount++;
        }
        else
        {
            bTutorialJump = true;
            onJumpTutorial.Invoke();
        }

        if(bTutorialJump && JumpCount < 3)
        {
            Vector3 new_hazard_pos;
            if (MovingObject.sMovingObjects.Count != 0)
                new_hazard_pos.z = MovingObject.sMovingObjects[MovingObject.sMovingObjects.Count - 1].transform.position.z;
            else
                new_hazard_pos.z = gameObject.transform.position.z;
            new_hazard_pos.z += GlobalGameProperties.MinimumPatternIntervalDistance;

            new_hazard_pos.x = 0.0f;
            new_hazard_pos.y = 0.0f;

            uint patterns = PickPatternIndex();
            if (patterns == 1) patterns = 0;
            GameObject clone_hazard_pattern_prefab = SpawnableHazardPatterns[patterns];


            if (clone_hazard_pattern_prefab)
            {
                new_hazard_pos.x = -1 * GlobalGameProperties.LaneDistance;
                Instantiate(
                    clone_hazard_pattern_prefab,
                    new_hazard_pos,
                    Quaternion.identity
                );

                new_hazard_pos.x = 0 * GlobalGameProperties.LaneDistance;
                Instantiate(
                   clone_hazard_pattern_prefab,
                   new_hazard_pos,
                   Quaternion.identity
               );

               new_hazard_pos.x = 1 * GlobalGameProperties.LaneDistance;
               Instantiate(
                   clone_hazard_pattern_prefab,
                   new_hazard_pos,
                   Quaternion.identity
               );
            }

            JumpCount++;
        }

        if (JumpCount >= 3)
        {
            bTutorial = false;
            onMovementTutorialFinish.Invoke();
        }

    }

    private void SpawnCoins()
    {
        Vector3 new_hazard_pos;
        if (MovingObject.sMovingObjects.Count != 0)
            new_hazard_pos.z = MovingObject.sMovingObjects[MovingObject.sMovingObjects.Count - 1].transform.position.z;
        else
            new_hazard_pos.z = gameObject.transform.position.z;
        new_hazard_pos.z += GlobalGameProperties.MinimumPatternIntervalDistance;

        new_hazard_pos.x = 0.0f;
        new_hazard_pos.y = 0.0f;

        GameObject clone_hazard_pattern_prefab = SpawnableHazardPatterns[1];

        if (clone_hazard_pattern_prefab)
        {
            Assets.Scripts.Hazards.Pattern pattern_script = null;
            bool result = clone_hazard_pattern_prefab.TryGetComponent<Assets.Scripts.Hazards.Pattern>(out pattern_script);
            if (result && !pattern_script.MiddleLaneOnly)
            {
                int lane_offset_int = Random.Range(-1, 2); //where to position them
                new_hazard_pos.x = lane_offset_int * GlobalGameProperties.LaneDistance;
            }

            Instantiate(
                clone_hazard_pattern_prefab,
                new_hazard_pos,
                Quaternion.identity
            );
        }
    }

    private void SpawnPattern()
    {
        Vector3 new_hazard_pos;
        if (MovingObject.sMovingObjects.Count != 0)
            new_hazard_pos.z = MovingObject.sMovingObjects[MovingObject.sMovingObjects.Count - 1].transform.position.z;
        else
            new_hazard_pos.z = gameObject.transform.position.z;
        new_hazard_pos.z += GlobalGameProperties.MinimumPatternIntervalDistance;

        new_hazard_pos.x = 0.0f;
        new_hazard_pos.y = 0.0f;

        GameObject clone_hazard_pattern_prefab = SpawnableHazardPatterns[PickPatternIndex()];

        if(clone_hazard_pattern_prefab)
        {
            Assets.Scripts.Hazards.Pattern pattern_script = null;
            bool result = clone_hazard_pattern_prefab.TryGetComponent<Assets.Scripts.Hazards.Pattern>(out pattern_script);
            if (result && !pattern_script.MiddleLaneOnly)
            {
                int lane_offset_int = Random.Range(-1, 2); //where to position them
                new_hazard_pos.x = lane_offset_int * GlobalGameProperties.LaneDistance;
            }

            Instantiate(
                clone_hazard_pattern_prefab,
                new_hazard_pos, 
                Quaternion.identity
            );
        }
    }

    private uint PickPatternIndex()
    {
        float rand = Random.Range(0.0f, sTotalWeight);
        float floor_rand = 0.0f;
        for (uint i = 0; i < SpawnableHazardPatterns.Length; ++i)
        {
            float curr_weight = SpawnableHazardPatterns[i].GetComponent<Assets.Scripts.Hazards.Pattern>().Weight;
            if (rand >= floor_rand && rand < floor_rand + curr_weight)
                return i;
            else
                floor_rand += curr_weight;
        }
        return 0;
    }

    public void onTurotialPatterns()
    {
        bTutorial = true;
    }

    public void onCoinCollectionTutorial()
    {
        bCoinCollectionTutorial = true;
    }
}
