﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MilkPowerHUD : MonoBehaviour
{
    public RectTransform bar2_background;
    private RectTransform self;
    public Image self_image;

    public PlayerMovement Player;

    // Start is called before the first frame update
    void Start()
    {
        self = GetComponent<RectTransform>();
        self.sizeDelta = new Vector2(bar2_background.sizeDelta.x, bar2_background.sizeDelta.y);
        self.anchoredPosition = bar2_background.anchoredPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float percentage = (Player.MilkCurrTime / Player.MilkMaxTime);

        // Turn green
        if (percentage <= 0.33f)
            self_image.color = new Color32(0, 255, 0, 100);
        // Turn Red
        else if (percentage > 0.66f)
            self_image.color = new Color32(255, 0, 0, 100);
        // Turn yellow
        else if (percentage > 0.33f)
            self_image.color = new Color32(255, 255, 0, 100);



        float height = percentage * bar2_background.sizeDelta.y;
        self.sizeDelta = new Vector2(bar2_background.sizeDelta.x, height);
    }
}
