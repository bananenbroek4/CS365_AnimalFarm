﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    class GlobalGameProperties
    {
        public static float LaneDistance = 2.5f;

        public static float HazardMoveSpeed = 7.5f;
        public static float MinimumPatternIntervalDistance = 10.0f;
        public static int SpawnPatternBelowObjectTreshhold = 6;
    }

}
