﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class CoinBehaviour : MovingObject
{
    private CurrencyManager manager;
    Rigidbody mRigid;
    // Start is called before the first frame update
    void Start()
    {
        AddMovingObject();
        GameObject mgr = GameObject.Find("Manager");
        manager = mgr.GetComponent<CurrencyManager>();
        mRigid = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            manager.AddCurrency();
            Destroy(gameObject);
            RemoveMovingObject();
        }
    }

    private void OnDestroy()
    {
        RemoveMovingObject();
    }
}
