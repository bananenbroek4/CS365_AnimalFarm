﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.VersionControl;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class CurrencyManager : MonoBehaviour
{
    private int currency;
    public int totalCurrency;
    public float startSpeed;
    public float endSpeed;
    // Start is called before the first frame update
    void Start()
    {
        currency = 0;

        bTutorial = bMovementTutorial = bcoinSpawn = bFhase1 = bFhase2 = bFhase3 = false;

        // Create a temporary reference to the current scene.
        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;
        if (sceneName == "Tutorial")
        {
            bTutorial = true;
        }

    }

    public float DoubleJumpUnlockPercentage;    //rabbit
    public float ShieldUnlockPercentage;        //sheep
    public float SpeedBoostUnlockPercentage;    //horse
    public float EggThrowUnlockPercentage;      //chicken
    public float ShootingMilkUnlockPercentage;  //cow

    public UnityEvent OnShieldUnlocked;
    public UnityEvent OnDoubleJumpUnlocked;
    public UnityEvent OnSpeedBoostUnlock;
    public UnityEvent OnEggThrowUnlocked;
    public UnityEvent OnShootingMilkUnlock;

    public UnityEvent OnFinish;

    bool bShieldUnlocked = false;       //rabbit
    bool bDoubleJumpUnlocked = false;   //sheep
    bool bSpeedBoostUnlock = false;      //horse
    bool bEggThrowUnlocked = false;    //chicken
    bool bShootingMilkUnlock = false;    //cow

    bool bTutorial;
    bool bMovementTutorial;
    bool bcoinSpawn;
    //turoial levels
    bool bFhase1;
    bool bFhase2;
    bool bFhase3;

    public UnityEvent OnFhase1;
    public UnityEvent OnFhase2;
    public UnityEvent OnFhase3;
    public UnityEvent OnPowerUpExplain;
    


    private void Update()
    {
        if (bTutorial)
        {
            UpdateTutorial();
            return;
        }

        //If ending, sets speed to 0.
        if (currency == totalCurrency)
        {
            GlobalGameProperties.HazardMoveSpeed = 0;
            return;
        }
        GlobalGameProperties.HazardMoveSpeed = startSpeed + GetCurrencyPercentage() * (endSpeed - startSpeed);
        if (Input.GetKeyDown(KeyCode.E))
        {
            bShieldUnlocked = true;
            OnShieldUnlocked.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            bDoubleJumpUnlocked = true;
            OnDoubleJumpUnlocked.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.T))        {
            bShootingMilkUnlock = true;
            OnShootingMilkUnlock.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            AddCurrency();
        }
    }

    private void UpdateTutorial()
    {
        //If ending, sets speed to 0.
        if (currency == totalCurrency)
        {
            GlobalGameProperties.HazardMoveSpeed = 0;
            return;
        }
        GlobalGameProperties.HazardMoveSpeed = startSpeed + GetCurrencyPercentage() * (endSpeed - startSpeed);

        if (Input.GetKeyDown(KeyCode.I))
        {
            AddCurrency();
        }

        if (!bFhase1)
        {
            bFhase1 = true;
            OnFhase1.Invoke();
        }
        if (bMovementTutorial && !bFhase2)
        {
            bFhase2 = true;
            OnFhase2.Invoke();
            bcoinSpawn = true;
        }
        if (bcoinSpawn && !bFhase3)
        {
            bFhase3 = true;
            OnFhase3.Invoke();
        }

        if (currency == totalCurrency / 2)
            OnPowerUpExplain.Invoke();
    }

    public void AddCurrency()
    {
        currency++;

        float currencyPercentage = GetCurrencyPercentage();

        if (!bShieldUnlocked && currencyPercentage > ShieldUnlockPercentage)
        {
            bShieldUnlocked = true;
            OnShieldUnlocked.Invoke();
        }
        if (!bDoubleJumpUnlocked && currencyPercentage > DoubleJumpUnlockPercentage)
        {
            bDoubleJumpUnlocked = true;
            OnDoubleJumpUnlocked.Invoke();
        }
        if (!bSpeedBoostUnlock && currencyPercentage > SpeedBoostUnlockPercentage)
        {
            bSpeedBoostUnlock = true;
            OnSpeedBoostUnlock.Invoke();
        }
        if (!bShootingMilkUnlock && currencyPercentage > ShootingMilkUnlockPercentage)
        {
            bShootingMilkUnlock = true;
            OnShootingMilkUnlock.Invoke();
        }
        if (!bEggThrowUnlocked && GetCurrencyPercentage() > EggThrowUnlockPercentage)
        {
            bEggThrowUnlocked = true;
            OnEggThrowUnlocked.Invoke();
        }
        if (currency == totalCurrency)
            OnFinish.Invoke();
    }

    public float GetCurrencyPercentage()
    {
        return ((float)currency) / ((float)totalCurrency);
    }

    public void MovementTutorialFinish ()
    {
        bMovementTutorial = true;
    }
}
