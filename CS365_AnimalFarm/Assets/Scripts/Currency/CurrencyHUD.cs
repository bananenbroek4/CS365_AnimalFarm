﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyHUD : MonoBehaviour
{
    public RectTransform bar_background;
    private RectTransform self;
    private CurrencyManager manager;

    // Start is called before the first frame update
    void Start()
    {
        self = GetComponent<RectTransform>();
        GameObject mgr = GameObject.Find("Manager");
        manager = mgr.GetComponent<CurrencyManager>();
        self.sizeDelta = new Vector2(bar_background.sizeDelta.x, 0);
    }

    // Update is called once per frame
    void Update()
    {
        float height = manager.GetCurrencyPercentage() * bar_background.sizeDelta.y;
        self.sizeDelta = new Vector2(bar_background.sizeDelta.x, height);
        self.anchoredPosition = bar_background.anchoredPosition + new Vector2(0, -bar_background.rect.height / 2 + self.rect.height / 2);
    }
}
