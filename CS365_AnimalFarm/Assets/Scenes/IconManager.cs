﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconManager : MonoBehaviour
{
    //link to the canvas
    public GameObject Canvas = null;
    
    //have the different textures prefabs
    public GameObject DoubleJump = null;
    public GameObject Shield = null;
    public GameObject Speed = null;
    public GameObject Egg = null;
    public GameObject Milk = null;

    //prefav of the bar
    public GameObject Bar;

    // Start is called before the first frame update
    void Start()
    {
        //Set a var with them 
        CreateObject(Speed, this.gameObject.GetComponent<CurrencyManager>().SpeedBoostUnlockPercentage / 1,70);
        CreateObject(Bar, this.gameObject.GetComponent<CurrencyManager>().SpeedBoostUnlockPercentage / 1);

        CreateObject(DoubleJump, this.gameObject.GetComponent<CurrencyManager>().DoubleJumpUnlockPercentage / 1, 70);
        CreateObject(Bar, this.gameObject.GetComponent<CurrencyManager>().DoubleJumpUnlockPercentage / 1);

        CreateObject(Shield, this.gameObject.GetComponent<CurrencyManager>().ShieldUnlockPercentage / 1, 70);
        CreateObject(Bar, this.gameObject.GetComponent<CurrencyManager>().ShieldUnlockPercentage / 1);

        CreateObject(Egg, this.gameObject.GetComponent<CurrencyManager>().EggThrowUnlockPercentage / 1, 70);
        CreateObject(Bar, this.gameObject.GetComponent<CurrencyManager>().EggThrowUnlockPercentage / 1);

        CreateObject(Milk, this.gameObject.GetComponent<CurrencyManager>().ShootingMilkUnlockPercentage / 1, 70);
        CreateObject(Bar, this.gameObject.GetComponent<CurrencyManager>().ShootingMilkUnlockPercentage / 1);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateObject(GameObject obj, float n, float x = 0)
    {

        //Set every icon to its normalize value
        Vector3 pos = new Vector3(x, -0.5f * Canvas.GetComponent<RectTransform>().sizeDelta.y, 0.01f);

        pos.y += n * Canvas.GetComponent<RectTransform>().sizeDelta.y;

        var o = Instantiate(obj, pos, Quaternion.LookRotation(new Vector3(0,0,1)));
        o.transform.SetParent(Canvas.transform, false);




    }

}
